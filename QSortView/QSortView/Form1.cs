﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QSortView
{
    public partial class MainForm : Form
    {
        public int[] array;
        private Random random = new Random();
        public MainForm()
        {
            InitializeComponent();
        }

        private void ViewSort()
        {
            quicksort(array, 0, array.Length-1);
            for(int i = 0; i < array.Length; i++)
                dataGridViewMain.Rows[0].Cells[i].Value = array[i].ToString();
        }

        private void clearDataGridInOneRow(int numRow, int numCell)
        {
            for (int i = 0; i < numCell; i++)
                dataGridViewMain.Rows[numRow].Cells[numCell] = null;
        }

        private void button1_Click(object sender, EventArgs e)
        {
           

            if (dataGridViewMain.Rows[0].Cells[0].Value != null)
            {
                addInArray(array);
            }
            else
            {
                for (int i = 0; i < array.Length; i++)
                {
                    array[i] = random.Next(0, 100);
                    dataGridViewMain.Rows[0].Cells[i].Value = array[i].ToString();
                }
            }
            dataGridViewMain.Rows[2].Cells[0].Value = "Результат";
            dataGridViewMain.Rows[2].Cells[1].Value = "ниже";
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            makeQSort();
            for(int i = 0; i < array.Length; i++)
                dataGridViewMain.Rows[0].Cells[i].Value = array[i].ToString();
        }

        public int[] makeQSort()
        {
            if (array != null && array.Length != 0)
                quicksort(array, 0, array.Length - 1);
            return array;
        }

        private void viewArray(int[] array, int numRow, DataGridView dataGrid)
        {
            for (int i = 0; i < array.Length; i++)
                dataGrid.Rows[numRow].Cells[i].Value = array[i];
        }

        private void addInArray(int[] array)
        {
            for (int i = 0; i < dataGridViewMain.ColumnCount; i++)
                array[i] = Convert.ToInt32(dataGridViewMain.Rows[0].Cells[i].Value.ToString());
        }

        private void button2_Click(object sender, EventArgs e)
        {
            int countCell = addCountEl.Text != "" ? Convert.ToInt32(addCountEl.Text) : 10;
            array = new int[countCell];
            dataGridViewMain.ColumnCount = countCell;
            dataGridViewMain.RowCount = 5;
        }

        private int partition(int[] array, int start, int end)
        {
            int marker = start;                     
            for (int i = start; i <= end; i++)
                dataGridViewMain.Rows[0].Cells[i].Style.ForeColor = Color.Green;
            Thread.Sleep(1000); dataGridViewMain.Refresh();
            dataGridViewMain.Rows[1].Cells[start].Value = dataGridViewMain.Rows[0].Cells[marker].Value;
            dataGridViewMain.Rows[0].Cells[start].Style.ForeColor = Color.Red;
            viewArray(array, 3, dataGridViewMain);
            Thread.Sleep(1000); dataGridViewMain.Refresh();
            for (int i = start; i <= end; i++)
            {      
                dataGridViewMain.Rows[1].Cells[i].Value = dataGridViewMain.Rows[0].Cells[i].Value;
                Thread.Sleep(1000); dataGridViewMain.Refresh();
                if (array[i] <= array[end])
                {
                    int temp = array[marker]; // swap
                    array[marker] = array[i];
                    array[i] = temp;
                    marker += 1;
                    viewArray(array, 3, dataGridViewMain);
                    Thread.Sleep(1000); dataGridViewMain.Refresh();
                }
                if(i != start)
                    dataGridViewMain.Rows[1].Cells[i].Value = null;
            }
            for (int i = start; i <= end; i++)
                dataGridViewMain.Rows[0].Cells[i].Style.ForeColor = Color.Black;
            Thread.Sleep(1000); dataGridViewMain.Refresh();
            dataGridViewMain.Rows[1].Cells[start].Value = null;
            Thread.Sleep(1000); dataGridViewMain.Refresh();
            return marker - 1;
        }

        public void quicksort(int[] array, int start, int end)
        {
            if (start >= end)
            {
                return;
            }
            int pivot = partition(array, start, end);           
            quicksort(array, start, pivot - 1);
            quicksort(array, pivot + 1, end);
        }

       
    }
}
